CREATE DATABASE  IF NOT EXISTS `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `mydb`;
-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `boleta`
--

DROP TABLE IF EXISTS `boleta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `boleta` (
  `idBoleta` int(11) NOT NULL,
  `fechaBoleta` date NOT NULL,
  `Cliente_idCliente` int(11) NOT NULL,
  PRIMARY KEY (`idBoleta`),
  KEY `fk_Boleta_Cliente1_idx` (`Cliente_idCliente`),
  CONSTRAINT `fk_Boleta_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boleta`
--

LOCK TABLES `boleta` WRITE;
/*!40000 ALTER TABLE `boleta` DISABLE KEYS */;
INSERT INTO `boleta` VALUES (1,'2020-01-18',1),(2,'2020-01-18',4),(3,'2020-01-17',2),(4,'2020-01-17',3),(5,'2020-01-18',4),(6,'2020-01-18',5),(7,'2020-01-16',10),(8,'2020-01-16',9),(9,'2020-01-16',8),(10,'2020-01-17',7),(11,'2020-01-15',6),(12,'2020-01-15',5),(13,'2020-01-15',4),(14,'2020-01-15',3),(15,'2020-01-15',2);
/*!40000 ALTER TABLE `boleta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `nombreCliente` varchar(45) NOT NULL,
  `dniCliente` int(11) NOT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'Christian',46094277),(2,'Rosa',46094278),(3,'Maria',46094279),(4,'Juana',46094280),(5,'Pedro',46094281),(6,'Jorge',46094282),(7,'Luis',46094283),(8,'Felipe',46094284),(9,'Isabel',46094285),(10,'Erick',46094286);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productos` (
  `idProducto` int(11) NOT NULL,
  `nombreProducto` varchar(45) NOT NULL,
  PRIMARY KEY (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'leche'),(2,'pan'),(3,'arroz'),(4,'lenteja'),(5,'mantequilla'),(6,'papaya'),(7,'platano'),(8,'fideos'),(9,'lechuga'),(10,'Pollo');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos_has_boleta`
--

DROP TABLE IF EXISTS `productos_has_boleta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productos_has_boleta` (
  `Productos_idProducto` int(11) NOT NULL,
  `Boleta_idBoleta` int(11) NOT NULL,
  PRIMARY KEY (`Productos_idProducto`,`Boleta_idBoleta`),
  KEY `fk_Productos_has_Boleta_Boleta1_idx` (`Boleta_idBoleta`),
  KEY `fk_Productos_has_Boleta_Productos_idx` (`Productos_idProducto`),
  CONSTRAINT `fk_Productos_has_Boleta_Boleta1` FOREIGN KEY (`Boleta_idBoleta`) REFERENCES `boleta` (`idBoleta`),
  CONSTRAINT `fk_Productos_has_Boleta_Productos` FOREIGN KEY (`Productos_idProducto`) REFERENCES `productos` (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos_has_boleta`
--

LOCK TABLES `productos_has_boleta` WRITE;
/*!40000 ALTER TABLE `productos_has_boleta` DISABLE KEYS */;
INSERT INTO `productos_has_boleta` VALUES (9,1),(8,2),(6,3),(5,4),(4,5),(3,6),(2,7),(10,8),(9,9),(6,10),(8,10),(5,11),(7,11),(4,12),(3,13),(2,14),(1,15);
/*!40000 ALTER TABLE `productos_has_boleta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-18 12:47:41
